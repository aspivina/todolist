import React from 'react'
import PropTypes from 'prop-types'

import '../../assets/styles.css';

const TodoItem = ({ todo, onClick, onDelete }) => {
  const onClickHandler = (myId) => {
     onClick(myId)
  }
  const onDeleteHandler = (myId) => {
     onDelete(myId)
  }
  return (
    <div className="todoItem">
        <li 
            className="item"
            onClick={() => onClickHandler(todo.id)} 
            style={{textDecoration: todo.completed ? 'line-through' : 'none'}}
            >{todo.text}
        </li>
        <span className="close" onClick={() => onDeleteHandler(todo.id)} >x</span>
    </div>
  );
}

TodoItem.propTypes = {
  todo: PropTypes.shape({
            id: PropTypes.number.isRequired,
            completed: PropTypes.bool.isRequired,
            text: PropTypes.string.isRequired
        }).isRequired,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default TodoItem
import React from 'react'


let AddTodo = ({onChange}) => {
    const keyUpHandler = (evt) => {
      const ENTER = 13
      if (evt.which === ENTER && input.value) {
        // hit enter, create new item if field isn't empty
        onChange(input.value)
        input.value = ''
      } 
    }
    let input;
    return (
      <header className="header">
        <input 
          autoFocus
          className="new-todo" 
          placeholder="What needs to be done?" 
          ref = {node=>{input=node;}}
          onKeyDown={keyUpHandler}
        /> 
      </header>  
    );
  };

  export default AddTodo
import React from 'react'
import PropTypes from 'prop-types'

import TodoItem from '../TodoItem'

const TodoList = ({ todos, filter, onClick, onDelete }) => {
  const onClickHandler = (myId) => {
     onClick(myId)
  }
  const onDeleteHandler = (myId) => {
     onDelete(myId)
  }
const getVisibleTodos = (todos, filter) => {
  console.log(filter);
  switch (filter) {
    case 'SHOW_ALL':
      return todos;
    case 'SHOW_COMPLETED':
      return todos.filter(t => t.completed);
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed);
    default:
      return todos;
  }
};
  return (
    <div className="todoList">
      <ul className="list">
        {getVisibleTodos(todos,filter).map(item => (
          <div key={item.id} >
              <TodoItem 
                todo={item}
                onClick={() => onClickHandler(item.id)} 
                onDelete={() => onDeleteHandler(item.id)}
              />
          </div>
        ))}
      </ul>

    </div>
  );
}
TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onClick: PropTypes.func.isRequired
}

export default TodoList
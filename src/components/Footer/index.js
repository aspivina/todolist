import React from 'react'
// import PropTypes from 'prop-types'

const Footer = ({ onClick}) => {

  const onClickHandler = (filter) => {
     onClick(filter)
  }

  return (
    <div>
        <button onClick={() => onClickHandler('SHOW_ALL')}>All</button>
        <button onClick={() => onClickHandler('SHOW_ACTIVE')}>Active</button>
        <button onClick={() => onClickHandler('SHOW_COMPLETED')}>Completed</button>
    </div>

  );
}


// TodoList.propTypes = {
//   todos: PropTypes.arrayOf(
//     PropTypes.shape({
//       id: PropTypes.number.isRequired,
//       completed: PropTypes.bool.isRequired,
//       text: PropTypes.string.isRequired
//     }).isRequired
//   ).isRequired,
//   onClick: PropTypes.func.isRequired
// }

export default Footer
import React from 'react';
import ListScreen from './screens/ListScreen'

import './App.css';

function App() {
  return (
    <div className="App">
      <ListScreen />
    </div>
  );
}

export default App;

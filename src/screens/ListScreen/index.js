import React from 'react';
import { connect } from 'react-redux';
import { addTodo,toggleTodo, deleteTodo,setVisibilityFilter } from '../../store/actions' 


import ListScreen from './listScreen'

const mapStateToProps = (state) => {
    return {
      todos: state.todos,
      filter: state.visibilityFilter,
    }
  }


const mapDispatchToProps = (dispatch) => {
    return {
        onAddTodo: (text) => { dispatch(addTodo(text))},
        onToggleTodo: (id) => { dispatch(toggleTodo(id))},
        onDeleteTodo: (id) => { dispatch(deleteTodo(id))},
        onVisibilityFilter: (filter) => {dispatch(setVisibilityFilter(filter))}
    }
  }



export default connect(mapStateToProps,mapDispatchToProps)(ListScreen);





import React from 'react';
import AddTodo from '../../components/AddTodo';
import TodoList from '../../components/TodoList';
import Footer from '../../components/Footer';



const ListScreen = ({todos,filter, onAddTodo, onToggleTodo, onDeleteTodo, onVisibilityFilter}) => {
    return (
       <div>
           <h1>-.Todo List.-</h1>
            <AddTodo onChange={value => onAddTodo(value)} />
            <Footer onClick={newFilter => onVisibilityFilter(newFilter)}/>
            <TodoList   todos={todos} 
                        filter={filter}
                        onClick={id => onToggleTodo(id)}
                        onDelete={id=> onDeleteTodo(id)}/>
             {/* <div>{todos[1] ? todos[1].text : ''}</div> */}
                
        </div>


    )
};

export default ListScreen;


